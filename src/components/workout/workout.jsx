import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";
import icon_bicep from "../../media/images/strong.svg";
//import triceps from "../../media/images/triceps.jpg";
import piernas from "../../media/images/pierna.jpg";
import pecho from "../../media/images/fit.svg";
//import M from "materialize-css";

class Workout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dat1: [],
      p: "",
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    //let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
  }

  async machines() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/machines/getmachines", datasend)
      .then((response) => response.json())
      .then((data1) => this.setState({ data1 }));
    console.log(this.state.data1);
  }

  async componentDidMount() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/plan_machines/workout", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.machines();
  }

  render() {
    const dato = this.state.data ? this.state.data : [];
    const dato1 = this.state.data1 ? this.state.data1 : [];
    var pechu = pecho;
    return (
      <div>
        <Menu />
        <div className="container">
          <div className="aca">
            {dato1.map(function (currentData, index) {
              return (
                <div className="tooltipp">
                  <img
                    alt=""
                    key={index + "d"}
                    src={
                      currentData.tipos_desc === "pecho"
                        ? pechu
                        : currentData.tipos_desc === "biceps"
                        ? icon_bicep
                        : currentData.tipos_desc === "piernas"
                        ? piernas
                        : null
                    }
                    id="btn-arm"
                    class="tooltipped btn-floating btn-large waves-effect waves-light "
                  ></img>
                  <span className="tooltiptext">{currentData.tipos_desc}</span>
                </div>
              );
            })}
          </div>
          <div className="exercises">
            {dato.map(function (currentData, index) {
              return (
                <div className="tooltipp">
                  <img
                    alt=""
                    key={index + "d"}
                    src={require("../../media/images/" +
                      currentData.machine_photo)}
                    id="site-1"
                    class="tooltipped btn-floating btn-large waves-effect waves-light "
                  ></img>
                  <span className="tooltiptext">
                    {currentData.machine_name}
                  </span>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Workout;
