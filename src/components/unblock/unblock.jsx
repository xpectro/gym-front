import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import BarMenu from "../squema/barmenu";
import Auth from "../../middleware/auth";
import bien from "../../media/images/bien.png";

class Unblock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      data: [],
      data1: [],
      data2: [],
      ejem: "",
    };
  }

  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
  }

  async componentDidMount() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/users/getblock", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  async pay(u) {
    let user_id = u;
    let d = new Date();
    let time = this.sumarDias(d, 30).toISOString().slice(0, 10);
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        pay_date: time,
        user_type: "2",
      }),
      headers: {
        "Content-type": "application/json",
        user_id: user_id,
      },
    };
    await fetch("http://localhost:3050/users/renewpay/" + user_id, datasend)
      .then((response) => response.json())
      .then((data2) => console.log(data2));
    this.componentDidMount();
  }

  superFuncion() {
    let data = this.state.data;
    if (data.length === 0) {
      return (
        <div>
          <BarMenu />
          <h4>
            Block <b style={{ color: "#bf360c" }}>Users</b>
          </h4>
          <div className="container">
            <div>
              <img src={bien} alt="img_bien" width="300px" />
            </div>
            <div className="mensaje">
              <h3>You Haven't Block Users</h3>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <BarMenu />
          <h4>
            Block <b style={{ color: "#bf360c" }}>Users</b>
          </h4>
          <div className="container">
            <table className="centered highlight">
              <thead>
                <tr>
                  <th>Document</th>
                  <th>Username</th>
                  <th>Pay_date</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                {data.map((currentData, index) => {
                  return (
                    <tr key={index + "d0"}>
                      <td key={index + "d1"}>{currentData.document}</td>
                      <td key={index + "d2"}>{currentData.username}</td>
                      <td key={index + "d3"}>
                        {new Date(currentData.pay_date)
                          .toISOString()
                          .slice(0, 10)}
                      </td>

                      <td key={index + "d12"}>
                        <button
                          onClick={() => this.pay(currentData.user_id)}
                          className="btn-floating btn-small waves-effect waves-light red"
                        >
                          <i className="material-icons">attach_money</i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      );
    }
  }

  render() {
    return <div>{this.superFuncion()}</div>;
  }
}

export default Unblock;
