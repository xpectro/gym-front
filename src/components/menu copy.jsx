import React from "react";
//import logo from "./logo.svg";
import "../index.css";
import { Component } from "react";
import BarMenu from "./squema/barmenu";
import Sound from "react-sound";
import mp3 from "../media/mp3/astronomia.mp3";
import p1 from "../media/images/1.jpg";
import p2 from "../media/images/index.jpg";
import p3 from "../media/images/2.jpg";
import p4 from "../media/images/3.jpeg";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Auth from "../middleware/auth";

const photos = [
  {
    name: "p1",
    url: p1,
  },
  {
    name: "p2",
    url: p2,
  },
  {
    name: "p3",
    url: p3,
  },
  {
    name: "p4",
    url: p4,
  },
];

class Menu extends Component {
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
  }

  render() {
    const settings = {
      dots: true,
      fade: true,
      infinite: true,
      speed: 500,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    };
    return (
      <div>
        <BarMenu />
        <h4>GYMNASIO EL PUTO BLANQUITO</h4>
        <div className="container">
          <div className="carousel">
            <Slider {...settings}>
              {photos.map((photo) => {
                return (
                  <div key="d0">
                    <img
                      src={photo.url}
                      width="100%"
                      height="100%"
                      alt={photo.name}
                    />
                  </div>
                );
              })}
            </Slider>
          </div>
          <div className="promo">
            <h5>PLANES DESDE $49.900 MÁS DE 500 SEDES</h5>

            <p className="text-promo">
              Facilidades Pensando en tu comodidad, contamos con diferentes
              servicios en línea como inscripción rápida, agendamiento de citas,
              transferencia de sedes y cambio de plan. Además del acompañamiento
              de nuestros profesores de planta, tendrás acceso a nuestro
              Entrenador Virtual para que entrenes de forma autónoma.
            </p>
          </div>
        </div>

        <Sound
          url={mp3}
          playStatus={Sound.status.PLAYING}
          playFromPosition={300}
          onLoading={this.handleSongLoading}
          onPlaying={this.handleSongPlaying}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
      </div>
    );
  }
}

export default Menu;
