import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";
import desayuno1 from "../../media/images/desayuno-para-adelgazar-1.jpg";
import desayuno2 from "../../media/images/desayuno-tortilla-dieta-adelgazamiento.jpg";
import desayuno3 from "../../media/images/desayuno-tortilla-espinacas-adelgazar.jpg";
import MidMorning1 from "../../media/images/midmorning1.jpg";
import MidMorning2 from "../../media/images/midmorning2.jpg";
import MidMorning3 from "../../media/images/midmorning3.jpg";
import Lunch1 from "../../media/images/lunch1.jpeg";
import Lunch2 from "../../media/images/lunch2.jpg";
import Lunch3 from "../../media/images/lunch3.jpg";
import MIdAfternoon1 from "../../media/images/midafternoon1.jpg";
import MIdAfternoon2 from "../../media/images/midafternoon2.jpg";
import MIdAfternoon3 from "../../media/images/midafternoon3.jpg";
import Dinner1 from "../../media/images/dinner1.jpg";
import Dinner2 from "../../media/images/dinner2.jpeg";
import Dinner3 from "../../media/images/dinner3.jpg";

class Tipo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayMenu: "",
      user_id: "",
      plan_type: "",
      recipeData: "",
      recipe_id: "",
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let doc = Auth.getUserId();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    } else {
      this.setState({ user_id: doc });
    }
  }

  componentDidMount() {
    this.getUserData();
  }

  async getRecipeData() {
    let recipe_id = this.state.recipe_id;
    const datasend = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        recipe_id: recipe_id,
      },
    };
    await fetch("http://localhost:3050/recipes/" + recipe_id, datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ recipeData: data }));
  }

  async getPlanData() {
    let plan_type = this.state.data[0]["plan_type"];
    const datasend = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        plan_type: plan_type,
      },
    };
    await fetch("http://localhost:3050/plans/plan_type/" + plan_type, datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ plan_type: data }));

    this.getRecipeData();
  }

  async getUserData() {
    let uid = this.state.user_id;
    const datasend = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        user_id: uid,
      },
    };
    await fetch("http://localhost:3050/users/" + uid, datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data: data }));

    this.getPlanData();
  }

  render() {
    return (
      <div>
        <Menu />
        <h4>
          My <b style={{ color: "#bf360c" }}>Recipes</b>
        </h4>
        <div className="container">
          <div className="con-recipes">
            <div className="links">
              <ul>
                <li>
                  <div
                    className="ee"
                    onClick={(e) => this.setState({ displayMenu: 1 })}
                  >
                    Breakfast
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <div
                    className="ee"
                    onClick={(e) => this.setState({ displayMenu: 2 })}
                  >
                    MidMorning
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <div
                    className="ee"
                    onClick={(e) => this.setState({ displayMenu: 3 })}
                  >
                    Lunch
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <div
                    className="ee"
                    onClick={(e) => this.setState({ displayMenu: 4 })}
                  >
                    MIdAfternoon
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <div
                    className="ee"
                    onClick={(e) => this.setState({ displayMenu: 5 })}
                  >
                    Dinner
                  </div>
                </li>
              </ul>
            </div>
            <div className="text">
              {this.state.displayMenu === 1 ? (
                <div>
                  <div className="imgtext">
                    <div className="cuadro1">
                      <img
                        src={desayuno1}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro2">
                      <img
                        src={desayuno2}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro3">
                      <img
                        src={desayuno3}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                  </div>
                  <div className="desctext">
                    <div className="desc1">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["breakfast"]}
                      </p>
                    </div>
                    <div className="desc2">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["breakfast2"]}
                      </p>
                    </div>
                    <div className="desc3">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["breakfast3"]}
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
              {this.state.displayMenu === 2 ? (
                <div>
                  <div className="imgtext">
                    <div className="cuadro1">
                      <img
                        src={MidMorning1}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro2">
                      <img
                        src={MidMorning2}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro3">
                      <img
                        src={MidMorning3}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                  </div>
                  <div className="desctext">
                    <div className="desc1">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midmorning"]}
                      </p>
                    </div>
                    <div className="desc2">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midmorning2"]}
                      </p>
                    </div>
                    <div className="desc3">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midmorning3"]}
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
              {this.state.displayMenu === 3 ? (
                <div>
                  <div className="imgtext">
                    <div className="cuadro1">
                      <img
                        src={Lunch1}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro2">
                      <img
                        src={Lunch2}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro3">
                      <img
                        src={Lunch3}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                  </div>
                  <div className="desctext">
                    <div className="desc1">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["lunch"]}
                      </p>
                    </div>
                    <div className="desc2">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["lunch2"]}
                      </p>
                    </div>
                    <div className="desc3">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["lunch3"]}
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
              {this.state.displayMenu === 4 ? (
                <div>
                  <div className="imgtext">
                    <div className="cuadro1">
                      <img
                        src={MIdAfternoon1}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro2">
                      <img
                        src={MIdAfternoon2}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro3">
                      <img
                        src={MIdAfternoon3}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                  </div>
                  <div className="desctext">
                    <div className="desc1">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midafternoon"]}
                      </p>
                    </div>
                    <div className="desc2">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midafternoon2"]}
                      </p>
                    </div>
                    <div className="desc3">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["midafternoon3"]}
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
              {this.state.displayMenu === 5 ? (
                <div>
                  <div className="imgtext">
                    <div className="cuadro1">
                      <img
                        src={Dinner1}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro2">
                      <img
                        src={Dinner2}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                    <div className="cuadro3">
                      <img
                        src={Dinner3}
                        alt="img1"
                        width="150px"
                        height="100px"
                      />
                    </div>
                  </div>
                  <div className="desctext">
                    <div className="desc1">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["dinner"]}
                      </p>
                    </div>
                    <div className="desc2">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["dinner2"]}
                      </p>
                    </div>
                    <div className="desc3">
                      <p style={{ fontSize: 13 }}>
                        {this.state.recipeData[0]["dinner3"]}
                      </p>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Tipo;
