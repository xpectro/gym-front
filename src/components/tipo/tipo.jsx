import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";

class Tipo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if(!admin){
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const datasend = {
      method: "GET",
    };
    fetch("http://localhost:3050/tipo", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div>
        <Menu />
        <h4>Config. Tipo</h4>
        <a
          href="/tipo/add"
          id="add"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">add</i>
        </a>
        <div className="container">
          <table className="centered highlight">
            <thead>
              <tr>
                <th>Tipo ID</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {dato.map(function (currentData, index) {
                return (
                  <tr key={index + "d0"}>
                    <td key={index + "d2"}>{currentData.tipo_id}</td>
                    <td key={index + "d3"}>{currentData.tipo_description}</td>
                    <td key={index + "d11"}>
                      <a
                        href={"/tipo/add/?" + currentData.tipo_id}
                        className="btn-floating btn-small waves-effect waves-light red"
                      >
                        <i className="material-icons">edit</i>
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Tipo;
