import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";

class FormularioTipo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      tipo_description: "",
      tipo_id: "",
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if(!admin){
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({ word: "Create" });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ tipo_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          tipo_id: url_data,
        },
      };
      console.log(datasend);
      fetch("http://localhost:3050/tipo/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ tipo_description: data[0]["tipo_description"] });
            console.log(data);
          }
        });
    }
  }
  aceptado = () => {
    window.location.href = "http://localhost:3000/tipo/";
  };

  validateForm = () => {
    return this.state.tipo_description.length > 0;
  };

  handleSubmit = (event) => {
    const { tipo_description } = this.state;
    const { tipo_id } = this.state;
    console.log({ tipo_id });
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        tipo_description: tipo_description,
      }),
      headers: {
        "Content-type": "application/json",
        tipo_id: this.state.tipo_id,
      },
    };
    console.log(this.state.tipo_id);
    if (this.state.tipo_id === "") {
      ruta = "http://localhost:3050/tipo/create";
    } else {
      ruta = "http://localhost:3050/tipo/modify/" + this.state.tipo_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        console.log(data.affectedRows);
        if (data.affectedRows) {
          //console.log("ok");
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          //console.log("not OK");
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    return (
      <div className="">
        <Menu />
        <a
          href="/tipo"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Tipo</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="input-field col s6">
                  <input
                    id="Description"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ tipo_description: e.target.value })
                    }
                    value={this.state.tipo_description}
                    required
                  />
                  <label htmlFor="Description">Description</label>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioTipo;
