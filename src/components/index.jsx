import React from "react";
//import logo from "./logo.svg";
import "../index.css";
import { Component } from "react";
//import Menu from './squema/menu'
import logo from "../media/images/logo.png";
import fondo from "../media/images/index.jpg";
import M from "materialize-css";
import Auth from "../middleware/auth";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document: "",
      password: "",
    };
  }

  handleSubmit = (e) => {
    const { document } = this.state;
    const { password } = this.state;

    const datasend = {
      method: "POST",
      body: JSON.stringify({
        document: document,
        password: password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch("http://localhost:3050/users/login", datasend)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        console.log(typeof data[0]);
        if (data[0] !== undefined) {
          console.log("CREDENCIALES INCORRECTAS");
          M.toast({ html: "Document or Pass Incorrect" });
        } else {
          if (data["user_type"] === 3) {
            M.toast({ html: "Your User is Blocked" });
          } else {
            console.log(data);
            Auth.setId_login(
              data["document"],
              data["username"],
              data["user_type"],
              data["user_id"]
            );
            Auth.login();
            window.location.href = "http://localhost:3000/menu/";
          }
        }
      })
      .catch(console.log);
    e.preventDefault();
  };

  validateForm = () => {
    return this.state.document.length > 0 && this.state.password.length > 0;
  };

  render() {
    return (
      <div>
        <img src={fondo} alt="fondo" className="img-init" />
        <div className="row">
          <div id="contenedor">
            <img src={logo} alt="" className="logo-init" />
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="input-field col s6">
                  <input
                    id="document"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ document: e.target.value })
                    }
                    value={this.state.document}
                    required
                  />
                  <label htmlFor="document">Document</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s6">
                  <input
                    id="password"
                    type="password"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ password: e.target.value })
                    }
                    value={this.state.password}
                    required
                  />
                  <label htmlFor="password">Password</label>
                </div>
              </div>
              <button
                className="waves-effect waves-light btn-small"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                Continue
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
