import React from "react";
//import logo from "./logo.svg";
import "../index.css";
import { Component } from "react";
import BarMenu from "./squema/barmenu";
import Auth from "../middleware/auth";
//import add from "./users/form_users";
import bien from "../media/images/bien.png";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "",
      data: [],
      data1: [],
      data2: [],
      ejem: "",
      title: "Strongs GYM",
    };
  }

  async getUser() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/users", datasend)
      .then((response) => response.json())
      .then((data1) => this.setState({ data1 }));
    this.inhabilitarUser();
  }

  inhabilitarUser() {
    let users = this.state.data1;
    let d = new Date();
    let time = d.toISOString().slice(0, 10);
    users.forEach((element) => {
      if (element["user_type"] !== "block" && element["pay_date"] !== null) {
        let t = element["pay_date"];
        let respuesta = t.toString().substring(0, 10);
        if (time > respuesta) {
          let user_id = element["user_id"];
          const datasend = {
            method: "POST",
            body: JSON.stringify({
              user_type: "3",
            }),
            headers: {
              "Content-type": "application/json",
              user_id: user_id,
            },
          };
          fetch("http://localhost:3050/users/block/" + user_id, datasend)
            .then((response) => response.json())
            .then((data1) => this.setState({ data1 }));
        }
      }
    });
  }

  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  componentWillMount() {
    document.title = this.state.title;
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/mysite";
    }
  }

  async componentDidMount() {
    let d = new Date();
    let time = this.sumarDias(d, 0).toISOString().slice(0, 10);
    this.setState({ date: time });
    const datasend = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        pay_date: time,
      },
    };
    await fetch(
      "http://localhost:3050/users/getpay/" + this.state.pay_date,
      datasend
    )
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.getUser();
  }

  async pay(u) {
    let user_id = u;
    let d = new Date();
    let time = this.sumarDias(d, 30).toISOString().slice(0, 10);
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        pay_date: time,
      }),
      headers: {
        "Content-type": "application/json",
        user_id: user_id,
      },
    };
    await fetch("http://localhost:3050/users/renew/" + user_id, datasend)
      .then((response) => response.json())
      .then((data2) => console.log(data2));
    this.componentDidMount();
  }

  superFuncion() {
    let data = this.state.data;
    if (data.length === 0) {
      return (
        <div>
          <BarMenu />
          <h4>
            Strongs <b style={{ color: "#bf360c" }}>GYM</b>
          </h4>
          <div className="container">
            <div>
              <img src={bien} alt="img_bien" width="300px" />
            </div>
            <div className="mensaje">
              <h3>You Haven't debtors</h3>
            </div>
          </div>
        </div>
      );
    } else {
      let elements = data.map((currentData, index) => {
        return (
          <div>
            <BarMenu />
            <h4>
              Gym <b style={{ color: "#bf360c" }}>Site</b>
            </h4>
            <div className="container">
              <table className="centered highlight">
                <thead>
                  <tr>
                    <th>Document</th>
                    <th>Username</th>
                    <th>Pay_date</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                <tbody>
                  <tr key={index + "d0"}>
                    <td key={index + "d1"}>{currentData.document}</td>
                    <td key={index + "d2"}>{currentData.username}</td>
                    <td key={index + "d3"}>
                      {new Date(currentData.pay_date)
                        .toISOString()
                        .slice(0, 10)}
                    </td>

                    <td key={index + "d12"}>
                      <button
                        onClick={() => this.pay(currentData.user_id)}
                        className="btn-floating btn-small waves-effect waves-light red"
                      >
                        <i className="material-icons">attach_money</i>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        );
      });
      return elements;
    }
  }

  render() {
    return <div>{this.superFuncion()}</div>;
  }
}

export default Menu;
