import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";
import Dropdown, { MenuItem } from "@trendmicro/react-dropdown";
import "@trendmicro/react-buttons/dist/react-buttons.css";
import "@trendmicro/react-dropdown/dist/react-dropdown.css";

class FormularioRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      document: "",
      username: "",
      birthdate: "",
      age: "",
      height: "",
      weight: "",
      genre: "",
      phone: "",
      address: "",
      email: "",
      password: "",
      chest: "",
      arm: "",
      forearm: "",
      musle: "",
      calf: "",
      waist: "",
      neck: "",
      hip: "",
      plan_type: "",
      user_type: "",
      create_time: "",
      user_id: "",
      data: [],
      data1: [],
      select_user_type: "",
      select_plan_type: "",
      pay_date: "",
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      let d = new Date();
      let time = this.sumarDias(d, 30).toISOString().slice(0, 10);
      this.setState({
        word: "Create",
        show: true,
        select_user_type: "USER TYPE",
        select_plan_type: "PLAN TYPE",
        genre: "GENRE",
        pay_date: time,
      });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ user_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          user_id: url_data,
        },
      };
      fetch("http://localhost:3050/users/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ document: data[0]["document"] });
            this.setState({ username: data[0]["username"] });
            //this.setState({ birthdate: data[0]["birthdate"] });
            this.setState({ age: data[0]["age"] });
            this.setState({ height: data[0]["height"] });
            this.setState({ weight: data[0]["weight"] });
            this.setState({ genre: data[0]["genre"] });
            this.setState({ phone: data[0]["phone"] });
            this.setState({ address: data[0]["address"] });
            this.setState({ email: data[0]["email"] });
            this.setState({ password: data[0]["password"] });
            this.setState({ chest: data[0]["chest"] });
            this.setState({ arm: data[0]["arm"] });
            this.setState({ forearm: data[0]["forearm"] });
            this.setState({ musle: data[0]["musle"] });
            this.setState({ calf: data[0]["calf"] });
            this.setState({ waist: data[0]["waist"] });
            this.setState({ neck: data[0]["neck"] });
            this.setState({ hip: data[0]["hip"] });
            this.setState({ create_time: data[0]["create_time"] });
            this.setState({ plan_type: data[0]["plan_type"] });
            this.setState({ user_type: data[0]["user_type"] });
            this.setState({ select_user_type: data[0]["user_type"] });
            this.setState({ select_plan_type: data[0]["plan_type"] });
            this.setState({ pay_date: data[0]["pay_date"] });
            //-----------------------------------------------------//
            let todayDate = new Date(data[0]["birthdate"])
              .toISOString()
              .slice(0, 10);
            this.setState({ birthdate: todayDate });
            //-----------------------------------------------------//
          }
        });
    }
    this.getTipos();
    this.getPlans();
  }
  aceptado = () => {
    window.location.href = "http://localhost:3000/users/";
  };

  /*validateForm = () => {
    return (
      this.state.document.length > 0 &&
      this.state.username.length > 0 &&
      this.state.birthdate.length > 0 &&
      this.state.age.length > 0 &&
      this.state.height.length > 0 &&
      this.state.weight.length > 0 &&
      this.state.genre.length > 0 &&
      this.state.phone.length > 0 &&
      this.state.address.length > 0 &&
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.plan_type > 0 &&
      this.state.user_type.length > 0
    );
  };*/

  async getTipos() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/tipos/user_type", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.state.data.forEach((element) => {
      if (element["tipos_desc"] === this.state.user_type) {
        this.setState({ user_type: element["tipos_id"] });
      }
    });
  }

  async getPlans() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/plans", datasend)
      .then((response) => response.json())
      .then((data1) => this.setState({ data1 }));
    this.state.data1.forEach((element) => {
      if (element["plan_name"] === this.state.plan_type) {
        this.setState({ plan_type: element["plan_id"] });
      }
    });
  }

  handleSubmit = (event) => {
    const { document } = this.state;
    const { username } = this.state;
    const { birthdate } = this.state;
    const { age } = this.state;
    const { height } = this.state;
    const { weight } = this.state;
    const { genre } = this.state;
    const { phone } = this.state;
    const { address } = this.state;
    const { email } = this.state;
    const { password } = this.state;
    const { chest } = this.state;
    const { arm } = this.state;
    const { forearm } = this.state;
    const { musle } = this.state;
    const { calf } = this.state;
    const { waist } = this.state;
    const { neck } = this.state;
    const { hip } = this.state;
    const { plan_type } = this.state;
    const { user_type } = this.state;
    const { pay_date } = this.state;
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        document: document,
        username: username,
        birthdate: birthdate,
        age: age,
        height: height,
        weight: weight,
        genre: genre,
        phone: phone,
        address: address,
        email: email,
        password: password,
        chest: chest,
        arm: arm,
        forearm: forearm,
        musle: musle,
        calf: calf,
        waist: waist,
        neck: neck,
        hip: hip,
        plan_type: plan_type,
        user_type: user_type,
        pay_date: pay_date,
      }),
      headers: {
        "Content-type": "application/json",
        user_id: this.state.user_id,
      },
    };
    if (this.state.word === "Create") {
      ruta = "http://localhost:3050/users/create";
    } else {
      ruta = "http://localhost:3050/users/modify/" + this.state.user_id;
    }
    console.log(datasend);
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        if (data.affectedRows) {
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          /*this.setState({
            document: "",
            username: "",
            birthdate: "",
            age: "",
            height: "",
            weight: "",
            genre: "",
            phone: "",
            address: "",
            email: "",
            password: "",
            chest: "",
            arm: "",
            forearm: "",
            musle: "",
            calf: "",
            waist: "",
            neck: "",
            hip: "",
            plan_type: "",
            user_type: "",
          });*/
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    const dato = this.state.data ? this.state.data : [];
    const dato1 = this.state.data1 ? this.state.data1 : [];
    return (
      <div className="">
        <Menu />
        <a
          href="/users"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Users</b>
        </h4>

        <div className="form" id="ver">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="personal">
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="document"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ document: e.target.value })
                      }
                      value={this.state.document}
                      required
                    />
                    <label htmlFor="document">Document</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="username"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ username: e.target.value })
                      }
                      value={this.state.username}
                      required
                    />
                    <label htmlFor="username">Username</label>
                  </div>

                  <div className="input-field col s12">
                    <input
                      id="birthdate"
                      type="date"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ birthdate: e.target.value })
                      }
                      value={this.state.birthdate}
                      required
                    />
                    <label htmlFor="birthdate">Birthdate</label>
                  </div>

                  <div className="input-field col s12">
                    <input
                      id="age"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ age: e.target.value })}
                      value={this.state.age}
                      required
                    />
                    <label htmlFor="age">Age</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="height"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ height: e.target.value })
                      }
                      value={this.state.height}
                      required
                    />
                    <label htmlFor="height">Height</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="weight"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ weight: e.target.value })
                      }
                      value={this.state.weight}
                      required
                    />
                    <label htmlFor="weight">Weight</label>
                  </div>

                  {/*<div className="input-field col s12">
                    <input
                      id="genre"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ genre: e.target.value })}
                      value={this.state.genre}
                      required
                    />
                    <label htmlFor="genre">Genre</label>
                    </div>*/}
                  <div className="input-field col s4">
                    <Dropdown
                      id="dropdown"
                      onSelect={(eventKey) => {
                        this.setState({
                          genre: eventKey,
                        });
                      }}
                    >
                      <Dropdown.Toggle
                        btnSize="lg"
                        btnStyle="active"
                        className="waves-effect waves-light deep-orange darken-4 btn-large"
                      >
                        {this.state.genre}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <MenuItem eventKey={"M"}>M</MenuItem>
                        <MenuItem eventKey={"F"}>F</MenuItem>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="phone"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ phone: e.target.value })}
                      value={this.state.phone}
                      required
                    />
                    <label htmlFor="phone">Phone</label>
                  </div>

                  <div className="input-field col s12">
                    <input
                      id="address"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ address: e.target.value })
                      }
                      value={this.state.address}
                      required
                    />
                    <label htmlFor="address">Address</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="email"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ email: e.target.value })}
                      value={this.state.email}
                      required
                    />
                    <label htmlFor="email">Email</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="password"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ password: e.target.value })
                      }
                      value={this.state.password}
                      required
                    />
                    <label htmlFor="password">Password</label>
                  </div>

                  {/*<div className="input-field col s12">
                    <input
                      id="plan_type"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ plan_type: e.target.value })
                      }
                      value={this.state.plan_type}
                      required
                    />
                    <label htmlFor="plan_type">Plan ID</label>
                  </div>*/}
                  <div className="input-field col s4">
                    <Dropdown
                      id="dropdown"
                      onSelect={(eventKey) => {
                        this.setState({
                          select_plan_type: eventKey[1],
                          plan_type: eventKey[0],
                        });
                      }}
                    >
                      <Dropdown.Toggle
                        btnSize="lg"
                        btnStyle="active"
                        className="waves-effect waves-light deep-orange darken-4 btn-large"
                      >
                        {this.state.select_plan_type}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {dato1.map(function (currentData) {
                          return (
                            <MenuItem
                              eventKey={[
                                currentData.plan_id,
                                currentData.plan_name,
                              ]}
                            >
                              {currentData.plan_name}
                            </MenuItem>
                          );
                        })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>

                  <div className="input-field col s4">
                    <Dropdown
                      id="dropdown"
                      onSelect={(eventKey) => {
                        this.setState({
                          select_user_type: eventKey[1],
                          user_type: eventKey[0],
                        });
                      }}
                    >
                      <Dropdown.Toggle
                        btnSize="lg"
                        btnStyle="active"
                        className="waves-effect waves-light deep-orange darken-4 btn-large"
                      >
                        {this.state.select_user_type}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {dato.map(function (currentData) {
                          return (
                            <MenuItem
                              eventKey={[
                                currentData.tipos_id,
                                currentData.tipos_desc,
                              ]}
                            >
                              {currentData.tipos_desc}
                            </MenuItem>
                          );
                        })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  {/*<div className="input-field col s12">
                    <input
                      id="user_type"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ user_type: e.target.value })
                      }
                      value={this.state.user_type}
                      required
                    />
                    <label htmlFor="user_type">User Type</label>
                    </div>*/}
                </div>
              </div>
              <div className="measures">
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="chest"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ chest: e.target.value })}
                      value={this.state.chest}
                      required
                    />
                    <label htmlFor="chest">Chest</label>
                  </div>

                  <div className="input-field col s12">
                    <input
                      id="arm"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ arm: e.target.value })}
                      value={this.state.arm}
                      required
                    />
                    <label htmlFor="arm">Arm</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="forearm"
                      type="text"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ forearm: e.target.value })
                      }
                      value={this.state.forearm}
                      required
                    />
                    <label htmlFor="forearm">Forearm</label>
                  </div>

                  <div className="input-field col s12">
                    <input
                      id="musle"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ musle: e.target.value })}
                      value={this.state.musle}
                      required
                    />
                    <label htmlFor="musle">Musle</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="calf"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ calf: e.target.value })}
                      value={this.state.calf}
                      required
                    />
                    <label htmlFor="calf">Calf</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="waist"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ waist: e.target.value })}
                      value={this.state.waist}
                      required
                    />
                    <label htmlFor="waist">Waist</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="neck"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ neck: e.target.value })}
                      value={this.state.neck}
                      required
                    />
                    <label htmlFor="neck">Neck</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="hip"
                      type="text"
                      className="validate"
                      onChange={(e) => this.setState({ hip: e.target.value })}
                      value={this.state.hip}
                      required
                    />
                    <label htmlFor="hip">Hip</label>
                  </div>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                id="btn_usu"
                name="action"
                /*disabled={!this.validateForm()}*/
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioRecipe;
