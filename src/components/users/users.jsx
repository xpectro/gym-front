import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      select: "",
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  async componentDidMount() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/users", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div>
        <Menu />
        <h4>Config. Users</h4>
        <a
          href="/users/add"
          id="add"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">add</i>
        </a>
        <div className="lol">
          <nav>
            <div className="nav-wrapper deep-orange darken-4">
              <ul id="nav-mobile" className="left hide-on-med-and-down">
                <li>
                  <a href onClick={(e) => this.setState({ select: 0 })}>
                    Personal Data
                  </a>
                </li>
                <li>
                  <a href onClick={(e) => this.setState({ select: 1 })}>
                    measurements
                  </a>
                </li>
              </ul>
            </div>
          </nav>

          {this.state.select === 0 ? (
            <div className="datos" style={{ overflowX: "auto" }}>
              <table className="centered highlight">
                <thead>
                  <tr>
                    <th>Document</th>
                    <th>Username</th>
                    <th>User Type</th>
                    <th>Birthdate</th>
                    <th>Age</th>

                    <th>Genre</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Plan Type</th>
                    <th>Pay Date</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                <tbody>
                  {dato.map(function (currentData, index) {
                    return (
                      <tr key={index + "c0"}>
                        <td key={index + "c2"}>{currentData.document}</td>
                        <td key={index + "c4"}>{currentData.username}</td>
                        <td key={index + "c4"}>{currentData.user_type}</td>
                        <td key={index + "c5"}>
                          {new Date(currentData.birthdate)
                            .toISOString()
                            .slice(0, 10)}
                        </td>
                        <td key={index + "c6"}>{currentData.age}</td>
                        <td key={index + "c9"}>{currentData.genre}</td>
                        <td key={index + "c10"}>{currentData.phone}</td>
                        <td key={index + "c11"}>{currentData.address}</td>
                        <td key={index + "c12"}>{currentData.email}</td>
                        <td key={index + "c13"}>{currentData.password}</td>
                        <td key={index + "c13"}>{currentData.plan_type}</td>
                        <td key={index + "c13"}>
                          {new Date(currentData.pay_date)
                            .toISOString()
                            .slice(0, 10)}
                        </td>
                        <td key={index + "c14"}>
                          <a
                            href={"/users/add/?" + currentData.user_id}
                            className="btn-floating btn-small waves-effect waves-light red"
                          >
                            <i className="material-icons">edit</i>
                          </a>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          ) : null}
          {this.state.select === 1 ? (
            <div className="datos">
              <table className="centered highlight">
                <thead>
                  <tr>
                    <th>Document</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Chest</th>
                    <th>Arm</th>
                    <th>Forearm</th>
                    <th>Musle</th>
                    <th>Calf</th>
                    <th>Waist</th>
                    <th>Neck</th>
                    <th>Hip</th>
                    <th>Create Time</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                <tbody>
                  {dato.map(function (currentData, index) {
                    return (
                      <tr key={index + "d0"}>
                        <td key={index + "d1"}>{currentData.document}</td>
                        <td key={index + "c7"}>{currentData.height}</td>
                        <td key={index + "c8"}>{currentData.weight}</td>
                        <td key={index + "d2"}>{currentData.chest}</td>
                        <td key={index + "d4"}>{currentData.arm}</td>
                        <td key={index + "d5"}>{currentData.forearm}</td>
                        <td key={index + "d6"}>{currentData.musle}</td>
                        <td key={index + "d7"}>{currentData.calf}</td>
                        <td key={index + "d8"}>{currentData.waist}</td>
                        <td key={index + "d9"}>{currentData.neck}</td>
                        <td key={index + "d10"}>{currentData.hip}</td>
                        <td key={index + "d11"}>{currentData.create_time}</td>
                        <td key={index + "d12"}>
                          <a
                            href={"/users/add/?" + currentData.user_id}
                            className="btn-floating btn-small waves-effect waves-light red"
                          >
                            <i className="material-icons">edit</i>
                          </a>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}
export default Users;
