import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";

class FormularioRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      recipe_id: "",
      breakfast: "",
      breakfast2: "",
      breakfast3: "",
      midmorning: "",
      midmorning2: "",
      midmorning3: "",
      lunch: "",
      lunch2: "",
      lunch3: "",
      midafternoon: "",
      midafternoon2: "",
      midafternoon3: "",
      dinner: "",
      dinner2: "",
      dinner3: "",
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({ word: "Create" });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ recipe_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          recipe_id: url_data,
        },
      };
      console.log(datasend);
      fetch("http://localhost:3050/recipes/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ breakfast: data[0]["breakfast"] });
            this.setState({ breakfast2: data[0]["breakfast2"] });
            this.setState({ breakfast3: data[0]["breakfast3"] });
            this.setState({ midmorning: data[0]["midmorning"] });
            this.setState({ midmorning2: data[0]["midmorning2"] });
            this.setState({ midmorning3: data[0]["midmorning3"] });
            this.setState({ lunch: data[0]["lunch"] });
            this.setState({ lunch2: data[0]["lunch2"] });
            this.setState({ lunch3: data[0]["lunch3"] });
            this.setState({ midafternoon: data[0]["midafternoon"] });
            this.setState({ midafternoon2: data[0]["midafternoon2"] });
            this.setState({ midafternoon3: data[0]["midafternoon3"] });
            this.setState({ dinner: data[0]["dinner"] });
            this.setState({ dinner2: data[0]["dinner2"] });
            this.setState({ dinner3: data[0]["dinner3"] });
            console.log(data);
          }
        });
    }
  }
  aceptado = () => {
    window.location.href = "http://localhost:3000/recipes/";
  };

  validateForm = () => {
    return (
      this.state.breakfast.length > 0 &&
      this.state.midmorning.length > 0 &&
      this.state.lunch.length > 0 &&
      this.state.midafternoon.length > 0 &&
      this.state.dinner.length > 0
    );
  };

  handleSubmit = (event) => {
    const { breakfast } = this.state;
    const { breakfast2 } = this.state;
    const { breakfast3 } = this.state;
    const { midmorning } = this.state;
    const { midmorning2 } = this.state;
    const { midmorning3 } = this.state;
    const { lunch } = this.state;
    const { lunch2 } = this.state;
    const { lunch3 } = this.state;
    const { midafternoon } = this.state;
    const { midafternoon2 } = this.state;
    const { midafternoon3 } = this.state;
    const { dinner } = this.state;
    const { dinner2 } = this.state;
    const { dinner3 } = this.state;
    const { recipe_id } = this.state;
    console.log({ recipe_id });
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        breakfast: breakfast,
        breakfast2: breakfast2,
        breakfast3: breakfast3,
        midmorning: midmorning,
        midmorning2: midmorning2,
        midmorning3: midmorning3,
        lunch: lunch,
        lunch2: lunch2,
        lunch3: lunch3,
        midafternoon: midafternoon,
        midafternoon2: midafternoon2,
        midafternoon3: midafternoon3,
        dinner: dinner,
        dinner2: dinner2,
        dinner3: dinner3,
      }),
      headers: {
        "Content-type": "application/json",
        recipe_id: this.state.recipe_id,
      },
    };
    console.log(this.state.recipe_id);
    if (this.state.recipe_id === "") {
      ruta = "http://localhost:3050/recipes/create";
    } else {
      ruta = "http://localhost:3050/recipes/modify/" + this.state.recipe_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        //console.log(data);
        if (data.affectedRows) {
          //console.log("ok");
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          //console.log(data.errno)
          /*this.setState({
            breakfast: "",
            midmorning: "",
            lunch: "",
            midafternoon: "",
            dinner: "",
          });*/
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    return (
      <div className="">
        <Menu />
        <a
          href="/recipes"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Recipes</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="input-field col s4">
                  <textarea
                    id="breakfast"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ breakfast: e.target.value })
                    }
                    value={this.state.breakfast}
                    required
                  />
                  <label htmlFor="breakfast">Breakfast</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="breakfast2"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ breakfast2: e.target.value })
                    }
                    value={this.state.breakfast2}
                    required
                  />
                  <label htmlFor="midmorning">Breakfast2</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="breakfast3"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ breakfast3: e.target.value })
                    }
                    value={this.state.breakfast3}
                    required
                  />
                  <label htmlFor="breakfast3">Breakfast3</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="midmorning"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midmorning: e.target.value })
                    }
                    value={this.state.midmorning}
                    required
                  />
                  <label htmlFor="midmorning">Midmorning</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="midmorning2"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midmorning2: e.target.value })
                    }
                    value={this.state.midmorning2}
                    required
                  />
                  <label htmlFor="midmorning2">Midmorning2</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s4">
                  <textarea
                    id="midmorning3"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midmorning3: e.target.value })
                    }
                    value={this.state.midmorning3}
                    required
                  />
                  <label htmlFor="midmorning3">Midmorning3</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="lunch"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ lunch: e.target.value })
                    }
                    value={this.state.lunch}
                    required
                  />
                  <label htmlFor="lunch">Lunch</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="lunch2"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ lunch2: e.target.value })
                    }
                    value={this.state.lunch2}
                    required
                  />
                  <label htmlFor="lunch2">Lunch2</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="lunch3"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ lunch3: e.target.value })
                    }
                    value={this.state.lunch3}
                    required
                  />
                  <label htmlFor="lunch3">Lunch3</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="midafternoon"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midafternoon: e.target.value })
                    }
                    value={this.state.midafternoon}
                    required
                  />
                  <label htmlFor="midafternoon">Midafternoon</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s4">
                  <textarea
                    id="midafternoon2"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midafternoon2: e.target.value })
                    }
                    value={this.state.midafternoon2}
                    required
                  />
                  <label htmlFor="midafternoon2">Midafternoon2</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="midafternoon3"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ midafternoon3: e.target.value })
                    }
                    value={this.state.midafternoon3}
                    required
                  />
                  <label htmlFor="midafternoon3">Midafternoon3</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="dinner"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ dinner: e.target.value })
                    }
                    value={this.state.dinner}
                    required
                  />
                  <label htmlFor="dinner">Dinner</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="dinner2"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ dinner2: e.target.value })
                    }
                    value={this.state.dinner2}
                    required
                  />
                  <label htmlFor="dinner2">Dinner2</label>
                </div>
                <div className="input-field col s4">
                  <textarea
                    id="dinner3"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ dinner3: e.target.value })
                    }
                    value={this.state.dinner3}
                    required
                  />
                  <label htmlFor="dinner3">Dinner3</label>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioRecipe;
