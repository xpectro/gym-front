import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";

class Recipes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if(!admin){
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const datasend = {
      method: "GET",
    };
    fetch("http://localhost:3050/recipes", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div>
        <Menu />
        <h4>Config. Recipes</h4>
        <a
          href="/recipes/add"
          id="add"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">add</i>
        </a>
        <div className="container" style={{overflowX:"auto"}}>
          <table className="centered highlight">
            <thead>
              <tr>
                <th>Recipe ID</th>
                <th>Breakfast</th>
                <th>Breakfast2</th>
                <th>Breakfast3</th>
                <th>Midmorning</th>
                <th>Midmorning2</th>
                <th>Midmorning3</th>
                <th>Lunch</th>
                <th>Lunch2</th>
                <th>Lunch3</th>
                <th>Midafternoon</th>
                <th>Midafternoon2</th>
                <th>Midafternoon3</th>
                <th>Dinner</th>
                <th>Dinner2</th>
                <th>Dinner3</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {dato.map(function (currentData, index) {
                return (
                  <tr key={index + "d0"}>
                    <td key={index + "d2"}>{currentData.recipe_id}</td>
                    <td key={index + "d3"}>{currentData.breakfast}</td>
                    <td key={index + "d4"}>{currentData.breakfast2}</td>
                    <td key={index + "d5"}>{currentData.breakfast3}</td>
                    <td key={index + "d6"}>{currentData.midmorning}</td>
                    <td key={index + "d7"}>{currentData.midmorning2}</td>
                    <td key={index + "d8"}>{currentData.midmorning3}</td>
                    <td key={index + "d9"}>{currentData.lunch}</td>
                    <td key={index + "d10"}>{currentData.lunch2}</td>
                    <td key={index + "d11"}>{currentData.lunch3}</td>
                    <td key={index + "d12"}>{currentData.midafternoon}</td>
                    <td key={index + "d13"}>{currentData.midafternoon2}</td>
                    <td key={index + "d14"}>{currentData.midafternoon3}</td>
                    <td key={index + "d15"}>{currentData.dinner}</td>                    
                    <td key={index + "d16"}>{currentData.dinner2}</td>                    
                    <td key={index + "d17"}>{currentData.dinner3}</td>                    
                    <td key={index + "d18"}>
                      <a
                        href={"/recipes/add/?" + currentData.recipe_id}
                        className="btn-floating btn-small waves-effect waves-light red"
                      >
                        <i className="material-icons">edit</i>
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Recipes;
