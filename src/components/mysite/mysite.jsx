import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";
import M from "materialize-css";

class Mysite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: "",
      chest: "",
      arm: "",
      forearm: "",
      musle: "",
      calf: "",
      waist: "",
      neck: "",
      hip: "",
      password: "",
      user_id: "",
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let doc = Auth.getUserId();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    } else {
      this.setState({ user_id: doc });
    }
    /*if(!admin){
      window.location.href = "http://localhost:3000/menu";
    }*/
  }

  aceptado = () => {
    window.location.href = "http://localhost:3000/menu/";
  };

  pass = (event) => {
    if (window.confirm("Are you sure of change password!")) {
      let { user_id } = this.state;
      let { password } = this.state;
      let ruta = "";
      const datasend = {
        method: "POST",
        body: JSON.stringify({
          password: password,
        }),
        headers: {
          "Content-type": "application/json",
          user_id: user_id,
        },
      };
      ruta = "http://localhost:3050/users/password/" + this.state.user_id;

      fetch(ruta, datasend)
        .then((response) => response.json())
        .then((data) => {
          //console.log(data);
          if (data.affectedRows) {
            //console.log("ok");
            M.toast({ html: "Task Ended Correctly" });
            setTimeout(this.aceptado, 2000);
          } else {
            //console.log(data.errno)
            this.setState({});
            let msjError = data.errno;
            this.setState({ mensaje: "Error:" + msjError });
            M.toast({ html: this.state.mensaje });
          }
        });
      event.preventDefault();
    }
  };

  measures = (event) => {
    if (window.confirm("Are you sure of change your measures!")) {
      const { user_id } = this.state;
      const { weight } = this.state;
      const { chest } = this.state;
      const { arm } = this.state;
      const { forearm } = this.state;
      const { musle } = this.state;
      const { calf } = this.state;
      const { waist } = this.state;
      const { neck } = this.state;
      const { hip } = this.state;
      let ruta = "";
      const datasend = {
        method: "POST",
        body: JSON.stringify({
          weight: weight,
          chest: chest,
          arm: arm,
          forearm: forearm,
          musle: musle,
          calf: calf,
          waist: waist,
          neck: neck,
          hip: hip,
        }),
        headers: {
          "Content-type": "application/json",
          user_id: user_id,
        },
      };
      ruta = "http://localhost:3050/users/measures/" + this.state.user_id;
      console.log(datasend);
      fetch(ruta, datasend)
        .then((response) => response.json())
        .then((data) => {
          //console.log(data);
          if (data.affectedRows) {
            //console.log("ok");
            M.toast({ html: "Task Ended Correctly" });
            setTimeout(this.aceptado, 2000);
          } else {
            //console.log(data.errno)
            this.setState({});
            let msjError = data.errno;
            this.setState({ mensaje: "Error:" + msjError });
            M.toast({ html: this.state.mensaje });
          }
        });
      event.preventDefault();
    }
  };

  componentDidMount() {
    const datasend = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        user_id: this.state.user_id,
      },
    };
    fetch("http://localhost:3050/users/" + this.state.user_id, datasend)
      .then((response) => response.json())
      .then((data) =>
        this.setState({
          weight: data[0]["weight"],
          chest: data[0]["chest"],
          arm: data[0]["arm"],
          forearm: data[0]["forearm"],
          musle: data[0]["musle"],
          calf: data[0]["calf"],
          waist: data[0]["waist"],
          neck: data[0]["neck"],
          hip: data[0]["hip"],
          password: data[0]["password"],
        })
      );
  }

  render() {
    return (
      <div>
        <Menu />
        <h4>
          My <b style={{ color: "#bf360c" }}>Site</b>
        </h4>
        <div className="container">
          <div class="row">
            <div class="col s12 m6">
              <div
                class="card blue-grey darken-2"
                id="ee"
                style={{ width: "25vw", borderRadius: 20 }}
              >
                <div class="card-content white-text">
                  <span class="card-title">Change Password</span>
                  <div className="input-field col s12">
                    <input
                      id="password"
                      type="password"
                      className="validate"
                      onChange={(e) =>
                        this.setState({ password: e.target.value })
                      }
                      value={this.state.password}
                      required
                    />
                    <label htmlFor="password">Password</label>
                  </div>
                </div>
                <div
                  class="card-action"
                  id="ee"
                  style={{ width: "24vw", borderRadius: 20 }}
                >
                  <button
                    className="btn waves-effect waves-light blue-grey darken-2"
                    onClick={this.pass}
                  >
                    Change
                  </button>
                </div>
              </div>
            </div>
            <div class="col s12 m6">
              <div
                class="card blue-grey darken-2"
                id="ee"
                style={{ width: "40vw", left: "-5vw", borderRadius: 20 }}
              >
                <div class="card-content white-text">
                  <span class="card-title">Change Measures</span>
                  <div className="row">
                    <div className="input-field col s12">
                      <input
                        id="weight"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ weight: e.target.value })
                        }
                        value={this.state.weight}
                        required
                      />
                      <label htmlFor="weight">Weight</label>
                    </div>

                    <div className="input-field col s12">
                      <input
                        id="chest"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ chest: e.target.value })
                        }
                        value={this.state.chest}
                        required
                      />
                      <label htmlFor="chest">Chest</label>
                    </div>

                    <div className="input-field col s12">
                      <input
                        id="arm"
                        type="text"
                        className="validate"
                        onChange={(e) => this.setState({ arm: e.target.value })}
                        value={this.state.arm}
                        required
                      />
                      <label htmlFor="arm">Arm</label>
                    </div>
                    <div className="input-field col s12">
                      <input
                        id="forearm"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ forearm: e.target.value })
                        }
                        value={this.state.forearm}
                        required
                      />
                      <label htmlFor="forearm">Forearm</label>
                    </div>
                    <div className="input-field col s12">
                      <input
                        id="musle"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ musle: e.target.value })
                        }
                        value={this.state.musle}
                        required
                      />
                      <label htmlFor="musle">Musle</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="input-field col s12">
                      <input
                        id="calf"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ calf: e.target.value })
                        }
                        value={this.state.calf}
                        required
                      />
                      <label htmlFor="calf">Calf</label>
                    </div>
                    <div className="input-field col s12">
                      <input
                        id="waist"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ waist: e.target.value })
                        }
                        value={this.state.waist}
                        required
                      />
                      <label htmlFor="waist">Waist</label>
                    </div>
                    <div className="input-field col s12">
                      <input
                        id="neck"
                        type="text"
                        className="validate"
                        onChange={(e) =>
                          this.setState({ neck: e.target.value })
                        }
                        value={this.state.neck}
                        required
                      />
                      <label htmlFor="neck">Neck</label>
                    </div>
                    <div className="input-field col s12">
                      <input
                        id="hip"
                        type="text"
                        className="validate"
                        onChange={(e) => this.setState({ hip: e.target.value })}
                        value={this.state.hip}
                        required
                      />
                      <label htmlFor="hip">Hip</label>
                    </div>
                  </div>
                </div>
                <div class="card-action" id="ee" style={{ borderRadius: 20 }}>
                  <button
                    className="btn waves-effect waves-light blue-grey darken-2"
                    onClick={this.measures}
                  >
                    Change
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Mysite;
