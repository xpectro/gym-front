import React from "react";
//import logo from "./logo.svg";
import "../../index.css";
import { Component } from "react";
import Menu from "../squema/barmenu";
import Auth from "../../middleware/auth";

class Plans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if(!admin){
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const datasend = {
      method: "GET",
    };
    fetch("http://localhost:3050/plans", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div>
        <Menu />
        <h4>Config. Plans</h4>
        <a
          href="/plans/add"
          id="add"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">add</i>
        </a>
        <div className="container">
          <table className="centered highlight">
            <thead>
              <tr>
                <th>Plan ID</th>
                <th>Recipe ID</th>
                <th>Plan Name</th>
                <th>Plan Type</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {dato.map(function (currentData, index) {
                return (
                  <tr key={index + "d0"}>
                    <td key={index + "d2"}>{currentData.plan_id}</td>
                    <td key={index + "d4"}>{currentData.recipe}</td>
                    <td key={index + "d5"}>{currentData.plan_name}</td>
                    <td key={index + "d6"}>{currentData.plan_type}</td>                   
                    <td key={index + "d11"}>
                      <a
                        href={"/plans/add/?" + currentData.plan_id}
                        className="btn-floating btn-small waves-effect waves-light red"
                      >
                        <i className="material-icons">edit</i>
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Plans;
