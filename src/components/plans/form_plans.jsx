import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";
import Dropdown, { MenuItem } from "@trendmicro/react-dropdown";
import "@trendmicro/react-buttons/dist/react-buttons.css";
import "@trendmicro/react-dropdown/dist/react-dropdown.css";

class FormularioPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      plan_id: "",
      plan_name: "",
      plan_type: "",
      recipe: "",
      select: "",
      data: [],
    };
  }

  async getTipos() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/tipos/plans_type", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    console.log(this.state.data);
    this.state.data.forEach((element) => {
      if (element["tipos_desc"] === this.state.plan_type) {
        this.setState({ plan_type: element["tipos_id"] });
      }
    });
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({ word: "Create", select: "PLAN TYPE" });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ plan_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          plan_id: url_data,
        },
      };
      console.log(datasend);
      fetch("http://localhost:3050/plans/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ plan_name: data[0]["plan_name"] });
            this.setState({ plan_type: data[0]["plan_type"] });
            this.setState({ recipe: data[0]["recipe"] });
            this.setState({ select: data[0]["plan_type"] });
          }
        });
    }
    this.getTipos();
  }

  aceptado = () => {
    window.location.href = "http://localhost:3000/plans/";
  };

  validateForm = () => {
    return (
      this.state.plan_name.length > 0 &&      
      this.state.recipe.length > 0
    );
  };

  handleSubmit = (event) => {
    const { plan_name } = this.state;
    const { plan_type } = this.state;
    const { recipe } = this.state;
    const { plan_id } = this.state;
    console.log({ plan_id });
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        plan_name: plan_name,
        plan_type: plan_type,
        recipe: recipe,
      }),
      headers: {
        "Content-type": "application/json",
        plan_id: this.state.plan_id,
      },
    };
    console.log(this.state.plan_id);
    if (this.state.plan_id === "") {
      ruta = "http://localhost:3050/plans/create";
    } else {
      ruta = "http://localhost:3050/plans/modify/" + this.state.plan_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        //console.log(data);
        if (data.affectedRows) {
          //console.log("ok");
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          //console.log(data.errno)
          this.setState({
            plan_name: "",
            plan_type: "",
            recipe: "",
          });
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div className="">
        <Menu />
        <a
          href="/plans"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Plans</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="input-field col s4">
                  <input
                    id="plan_name"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ plan_name: e.target.value })
                    }
                    value={this.state.plan_name}
                    required
                  />
                  <label htmlFor="plan_name">Plan Name</label>
                </div>
                {/*<div className="input-field col s4">
                  <input
                    id="plan_type"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ plan_type: e.target.value })
                    }
                    value={this.state.plan_type}
                    required
                  />
                  <label htmlFor="plan_type">Plan Type</label>
                  </div>*/}
                <div className="input-field col s4">
                  <Dropdown
                    id="dropdown"
                    onSelect={(eventKey) => {
                      this.setState({
                        select: eventKey[1],
                        plan_type: eventKey[0],
                      });
                    }}
                  >
                    <Dropdown.Toggle
                      btnSize="lg"
                      btnStyle="active"
                      className="waves-effect waves-light deep-orange darken-4 btn-large"
                    >
                      {this.state.select}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {dato.map(function (currentData, index) {
                        return (
                          <MenuItem
                            key={index}
                            eventKey={[
                              currentData.tipos_id,
                              currentData.tipos_desc,
                            ]}
                          >
                            {currentData.tipos_desc}
                          </MenuItem>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                <div className="input-field col s4">
                  <input
                    id="recipe"
                    type="text"
                    className="validate"
                    onChange={(e) => this.setState({ recipe: e.target.value })}
                    value={this.state.recipe}
                    required
                  />
                  <label htmlFor="recipe">Recipe</label>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioPlan;
