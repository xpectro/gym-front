import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";
import Dropdown, { MenuItem } from "@trendmicro/react-dropdown";
import "@trendmicro/react-buttons/dist/react-buttons.css";
import "@trendmicro/react-dropdown/dist/react-dropdown.css";

class FormularioPlan_Machines extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      plans_plan_id: "",
      machines_machine_id: "",
      plan_machines_id: "",
      data: [],
      data1: [],
      select_plan: "",
      select_machine: "",
      ver: false,
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({
        word: "Create",
        select_plan: "SELECT PLAN",
        select_machine: "SELECT MACHINE",
      });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ plan_machines_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          plan_machines_id: url_data,
        },
      };
      console.log(datasend);
      fetch("http://localhost:3050/plan_machines/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({
              machines_machine_id: data[0]["machines_machine_id"],
              select_machine: data[0]["machines_machine_id"],
              plans_plan_id: data[0]["plans_plan_id"],
              select_plan: data[0]["plans_plan_id"],
            });
          }
        });
    }
    this.getPlans();
    this.getMachines();
  }

  aceptado = () => {
    window.location.href = "http://localhost:3000/plans_machines/";
  };

  async getPlans() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/plans/", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.state.data.forEach((element) => {
      if (element["plan_name"] === this.state.plans_plan_id) {
        this.setState({ plans_plan_id: element["plan_id"] });
      }
    });
  }

  async getMachines() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/machines", datasend)
      .then((response) => response.json())
      .then((data1) => this.setState({ data1 }));
    this.state.data1.forEach((element) => {
      if (element["machine_name"] === this.state.machines_machine_id) {
        this.setState({ machines_machine_id: element["machine_id"] });
      }
    });
  }

  ver(){
    if(this.state.ver === false){
      this.setState({ver:true})
    }
    
  }

  handleSubmit = (event) => {
    const { plans_plan_id } = this.state;
    const { machines_machine_id } = this.state;
    console.log({ machines_machine_id });
    console.log({ plans_plan_id });
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        plans_plan_id: plans_plan_id,
        machines_machine_id: machines_machine_id,
      }),
      headers: {
        "Content-type": "application/json",
        plan_machines_id: this.state.plan_machines_id,
      },
    };
    console.log(datasend);
    if (this.state.plan_machines_id === "") {
      ruta = "http://localhost:3050/plan_machines/create";
    } else {
      ruta =
        "http://localhost:3050/plan_machines/modify/" +
        this.state.plan_machines_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        //console.log(data);
        if (data.affectedRows) {
          //console.log("ok");
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          //console.log(data.errno)
          this.setState({
            machines_machine_id: "",
            plans_plan_id: "",
            plan_machines_id: "",
          });
          this.ver()
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    const dato = this.state.data ? this.state.data : [];
    const dato1 = this.state.data1 ? this.state.data1 : [];
    return (
      <div className="">
        <Menu />
        <a
          href="/plans_machines"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Plans Machines</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                {/*<div className="input-field col s4">
                  <input
                    id="plans_plan_id"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ plans_plan_id: e.target.value })
                    }
                    value={this.state.plans_plan_id}
                    required
                  />
                  <label htmlFor="plans_plan_id">Plan Id</label>
                  </div>*/}
                <div className="input-field col s4">
                  <Dropdown
                    id="dropdown"
                    onSelect={(eventKey) => {
                      this.setState({
                        select_plan: eventKey[1],
                        plans_plan_id: eventKey[0],
                      });
                    }}
                  >
                    <Dropdown.Toggle
                      btnSize="lg"
                      btnStyle="active"
                      className="waves-effect waves-light deep-orange darken-4 btn-large"
                    >
                      {this.state.select_plan}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {dato.map(function (currentData, index) {
                        return (
                          <MenuItem
                            key={index}
                            eventKey={[
                              currentData.plan_id,
                              currentData.plan_name,
                            ]}
                          >
                            {currentData.plan_name}
                          </MenuItem>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                {/*<div className="input-field col s4">
                  <input
                    id="machines_machine_id"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ machines_machine_id: e.target.value })
                    }
                    value={this.state.machines_machine_id}
                    required
                  />
                  <label htmlFor="machines_machine_id">Machine Id</label>
                  </div>*/}
                <div className="input-field col s4">
                  <Dropdown
                    id="dropdown"
                    onSelect={(eventKey) => {
                      this.setState({
                        select_machine: eventKey[1],
                        machines_machine_id: eventKey[0],
                      });
                    }}
                  >
                    <Dropdown.Toggle
                      id="dropdown-btn"
                      btnSize="lg"
                      btnStyle="active"
                      className="waves-effect waves-light deep-orange darken-4 btn-large"
                    >
                      {this.state.select_machine}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {dato1.map(function (currentData, index) {
                        return (
                          <MenuItem
                            key={index}
                            eventKey={[
                              currentData.machine_id,
                              currentData.machine_name,
                            ]}
                          >
                            {currentData.machine_name}
                          </MenuItem>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={this.ver()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioPlan_Machines;
