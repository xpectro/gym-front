import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";
import Dropdown, { MenuItem } from "@trendmicro/react-dropdown";
import "@trendmicro/react-buttons/dist/react-buttons.css";
import "@trendmicro/react-dropdown/dist/react-dropdown.css";

class FormularioTipos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      tipos_desc: "",
      tipos_id: "",
      tipos_valor: "",
      tipo_tipo_id: "",
      uid: "",
      data: [],
      select: "",
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    //let uid = Auth.getUserId();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({ word: "Create", select: "Select Tipo" });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ tipos_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          tipos_id: url_data,
        },
      };
      fetch("http://localhost:3050/tipos/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ tipos_desc: data[0]["tipos_desc"] });
            this.setState({ tipos_valor: data[0]["tipos_valor"] });
            this.setState({ tipo_tipo_id: data[0]["tipo_tipo_id"] });
            this.setState({ select: data[0]["tipo_tipo_id"] });
          }
        });
    }
    this.getTipos();
  }

  aceptado = () => {
    window.location.href = "http://localhost:3000/tipos/";
  };

  validateForm = () => {
    return (
      this.state.tipos_desc.length > 0 && this.state.tipos_valor.length > 0
    );
  };

  async getTipos() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/tipo", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.state.data.forEach((element) => {
      if (element["tipo_description"] === this.state.tipo_tipo_id) {
        this.setState({ tipo_tipo_id: element["tipo_id"] });
      }
    });
  }

  handleSubmit = (event) => {
    const { tipos_desc } = this.state;
    const { tipo_tipo_id } = this.state;
    const { tipos_valor } = this.state;
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        tipos_desc: tipos_desc,
        tipo_tipo_id: tipo_tipo_id,
        tipos_valor: tipos_valor,
      }),
      headers: {
        "Content-type": "application/json",
        tipos_id: this.state.tipos_id,
      },
    };
    if (this.state.tipos_id === "") {
      ruta = "http://localhost:3050/tipos/create";
    } else {
      ruta = "http://localhost:3050/tipos/modify/" + this.state.tipos_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        if (data.affectedRows) {
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          this.setState({ tipos_desc: "", tipo_tipo_id: "", tipos_valor: "" });
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div className="">
        <Menu />
        <a
          href="/tipos"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Tipos</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="input-field col s4">
                  <Dropdown
                    id="dropdown"
                    onSelect={(eventKey) => {
                      this.setState({
                        select: eventKey[1],
                        tipo_tipo_id: eventKey[0],
                      });
                    }}
                  >
                    <Dropdown.Toggle btnSize="lg" btnStyle="active" className="waves-effect waves-light deep-orange darken-4 btn-large">
                      {this.state.select}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {dato.map(function (currentData, index) {
                        return (
                          <MenuItem
                            key={index}
                            eventKey={[
                              currentData.tipo_id,
                              currentData.tipo_description,
                            ]}
                          >
                            {currentData.tipo_description}
                          </MenuItem>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                {/*/<div className="input-field col s4">
                  <input
                    id="tipoId"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ tipo_tipo_id: e.target.value })
                    }
                    value={this.state.tipo_tipo_id}
                    required
                  />
                  <label htmlFor="tipoId">Tipo ID</label>
                </div>*/}
                <div className="input-field col s4">
                  <input
                    id="Description"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ tipos_desc: e.target.value })
                    }
                    value={this.state.tipos_desc}
                    required
                  />
                  <label htmlFor="Description">Description</label>
                </div>
                <div className="input-field col s4">
                  <input
                    id="value"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ tipos_valor: e.target.value })
                    }
                    value={this.state.tipos_valor}
                    required
                  />
                  <label htmlFor="value">Value</label>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioTipos;
