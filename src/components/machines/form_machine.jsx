import React, { Component } from "react";
import Menu from "../squema/barmenu";
import M from "materialize-css";
import "../../index.css";
import Auth from "../../middleware/auth";
import Dropdown, { MenuItem } from "@trendmicro/react-dropdown";
import "@trendmicro/react-buttons/dist/react-buttons.css";
import "@trendmicro/react-dropdown/dist/react-dropdown.css";

class FormularioMachine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      show: false,
      msg: "",
      machine_serial: "",
      machine_name: "",
      machine_type: "",
      machine_id: "",
      machine_photo: "",
      select: "",
      data: [],
    };
  }

  componentWillMount() {
    let auth = Auth.isAuthenticated();
    let admin = Auth.isAdmin();
    if (!auth) {
      window.location.href = "http://localhost:3000/";
    }
    if (!admin) {
      window.location.href = "http://localhost:3000/menu";
    }
  }

  componentDidMount() {
    const { search } = this.props.location;
    const size = { search }.search;
    if (size !== "") {
      this.setState({ word: "Update" });
    } else {
      this.setState({ word: "Create", select: "TYPE MACHINE" });
    }
    const url_data = search.replace("?", "");
    if (url_data) {
      this.setState({ machine_id: url_data });
      const datasend = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          machine_id: url_data,
        },
      };
      fetch("http://localhost:3050/machines/" + url_data, datasend)
        .then((response) => response.json())
        .then((data) => {
          if (data.length === 0) {
            M.toast({ html: "Invalid Search!" });
          } else {
            this.setState({ machine_serial: data[0]["machine_serial"] });
            this.setState({ machine_name: data[0]["machine_name"] });
            this.setState({ machine_type: data[0]["machine_type"] });
            this.setState({ machine_photo: data[0]["machine_photo"] });
            this.setState({ select: data[0]["machine_type"] });
          }
        });
    }
    this.getTipos();
  }

  file = (event) => {
    this.setState({ machine_photo: event.target.files[0].name });
  };

  aceptado = () => {
    window.location.href = "http://localhost:3000/machines/";
  };

  validateForm = () => {
    return (
      this.state.machine_serial.length > 0 && this.state.machine_name.length > 0
    );
  };

  async getTipos() {
    const datasend = {
      method: "GET",
    };
    await fetch("http://localhost:3050/tipos/machines_type", datasend)
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
    this.state.data.forEach((element) => {
      if (element["tipos_desc"] === this.state.machine_type) {
        this.setState({ machine_type: element["tipos_id"] });
      }
    });
  }

  handleSubmit = (event) => {
    const { machine_type } = this.state;
    const { machine_name } = this.state;
    const { machine_serial } = this.state;
    const { machine_photo } = this.state;
    let ruta = "";
    const datasend = {
      method: "POST",
      body: JSON.stringify({
        machine_type: machine_type,
        machine_name: machine_name,
        machine_serial: machine_serial,
        machine_photo: machine_photo,
      }),
      headers: {
        "Content-type": "application/json",
        machine_id: this.state.machine_id,
      },
    };
    if (this.state.machine_id === "") {
      ruta = "http://localhost:3050/machines/create";
    } else {
      ruta = "http://localhost:3050/machines/modify/" + this.state.machine_id;
    }
    fetch(ruta, datasend)
      .then((response) => response.json())
      .then((data) => {
        if (data.affectedRows) {
          this.setState({
            machine_serial: "",
            machine_name: "",
            machine_type: "",
          });
          M.toast({ html: "Task Ended Correctly" });
          setTimeout(this.aceptado, 2000);
        } else {
          this.setState({
            machine_serial: "",
            machine_name: "",
            machine_type: "",
            machine_photo: "",
          });
          let msjError = data.errno;
          this.setState({ mensaje: "Error:" + msjError });
          M.toast({ html: this.state.mensaje });
        }
      });
    event.preventDefault();
  };

  render() {
    const dato = this.state.data ? this.state.data : [];
    return (
      <div className="">
        <Menu />
        <a
          href="/machines"
          id="back"
          className="btn-floating btn-small waves-effect waves-light deep-orange darken-4"
        >
          <i className="material-icons">keyboard_backspace</i>
        </a>
        <h4>
          {this.state.word} <b style={{ color: "#bf360c" }}>Machines</b>
        </h4>
        <div className="form">
          <div className="row">
            <form className="col s12" onSubmit={this.handleSubmit}>
              <div className="row">
                <div class="file-field input-field">
                  <div className="btn">
                    <span>File</span>
                    <input onChange={this.file} required type="file" />
                  </div>
                  <div className="file-path-wrapper">
                    <input
                      className="file-path validate"
                      value={this.state.machine_photo}
                      type="text"
                    />
                  </div>
                </div>
                <div className="input-field col s4">
                  <input
                    id="serial"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ machine_serial: e.target.value })
                    }
                    value={this.state.machine_serial}
                    required
                  />
                  <label htmlFor="serial">Serial</label>
                </div>
                <div className="input-field col s4">
                  <input
                    id="name"
                    type="text"
                    className="validate"
                    onChange={(e) =>
                      this.setState({ machine_name: e.target.value })
                    }
                    value={this.state.machine_name}
                    required
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="input-field col s4">
                  <Dropdown
                    id="dropdown"
                    onSelect={(eventKey) => {
                      this.setState({
                        select: eventKey[1],
                        machine_type: eventKey[0],
                      });
                    }}
                  >
                    <Dropdown.Toggle
                      id="dropdown-btn"
                      btnSize="lg"
                      btnStyle="active"
                      className="waves-effect waves-light deep-orange darken-4 btn-large"
                    >
                      {this.state.select}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {dato.map(function (currentData, index) {
                        return (
                          <MenuItem
                            key={index}
                            eventKey={[
                              currentData.tipos_id,
                              currentData.tipos_desc,
                            ]}
                          >
                            {currentData.tipos_desc}
                          </MenuItem>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <button
                className="btn waves-effect waves-light blue-grey darken-1"
                type="submit"
                name="action"
                disabled={!this.validateForm()}
              >
                {this.state.word}
                <i className="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default FormularioMachine;
