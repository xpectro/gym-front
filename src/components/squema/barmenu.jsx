import React from "react";
import logo from "../../media/images/logo.png";
import "../../index.css";
import { Component } from "react";
//import Auth from "../../middleware/auth";
import icon from "../../media/images/face.png";

class BarMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      type: "",
      displayMenu: false,
    };

    this.showDropdownMenu = this.showDropdownMenu.bind(this);
    this.hideDropdownMenu = this.hideDropdownMenu.bind(this);
  }

  componentWillMount() {
    let username = sessionStorage.getItem("username");
    let type = sessionStorage.getItem("type");
    this.setState({ username: username, type: type });
  }

  showDropdownMenu(event) {
    event.preventDefault();
    this.setState({ displayMenu: true }, () => {
      document.addEventListener("click", this.hideDropdownMenu);
    });
  }

  hideDropdownMenu() {
    this.setState({ displayMenu: false }, () => {
      document.removeEventListener("click", this.hideDropdownMenu);
    });
  }

  exit() {
    if (window.confirm("Are you sure of Exit App!")) {
      window.location.href = "http://localhost:3000/close";
    }
  }

  render() {
    return (
      <div className="">
        <nav>
          <div className="nav-wrapper" id="menu-nav">
            <a href="/menu">
              <img src={logo} alt="" className="menu-logo" />
            </a>
            <ul className="left hide-on-med-and-down">
              {/*<li>
                <a href="/workout">Workout Plan</a>
              </li>*/}
              {this.state.type === "1" ? (
                <li>
                  <a href="/unblock">Unblock</a>
                </li>
              ) : null}
              <li>
                <div
                  className="dropdown"
                  style={{ background: "black", width: "100px" }}
                >
                  {this.state.type === "1" ? (
                    <a
                      href="/"
                      className="button"
                      onClick={this.showDropdownMenu}
                    >
                      <i className="material-icons">settings</i>
                    </a>
                  ) : null}

                  {this.state.displayMenu ? (
                    <div className="dd">
                      <ul>
                        <li>
                          <a href="/tipo">Tipo</a>
                        </li>
                        <li>
                          <a href="/tipos">Tipos</a>
                        </li>
                        <li>
                          <a href="/plans">Plans</a>
                        </li>
                        <li>
                          <a href="/plans_machines">Plans/M</a>
                        </li>
                        <li>
                          <a href="/recipes">Conf.Recipes</a>
                        </li>
                        <li>
                          <a href="/machines">Machines</a>
                        </li>
                        <li>
                          <a href="/users">Users</a>
                        </li>
                      </ul>
                    </div>
                  ) : null}
                </div>
              </li>
            </ul>
            <ul className="right hide-on-med-and-down">
              <li>
                <a style={{ fontSize: 16 }} href="/mysite">
                  <img src={icon} alt="" />
                  {this.state.username}
                </a>
              </li>
              <li>
                <a onClick={this.exit} href>
                  <i className="material-icons" style={{ marginRight: "15px" }}>
                    exit_to_app
                  </i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default BarMenu;
