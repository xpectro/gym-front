import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Auth from "../middleware/auth";

class CloseSesion extends Component {
  componentDidMount() {
    Auth.logout();
  }
  render() {
    return <Redirect to={{ pathname: "/" }} />;
  }
}
export default CloseSesion;
