import SimpleCrypto from "simple-crypto-js";

class Auth {
  constructor() {
    this.authenticated = false;
    this.id_login = "";
    this.username = "";
    this.type = "";
    this.secret_key = "1234";
    this.simpleCrypto = new SimpleCrypto(this.secret_key);
  }

  setId_login(document, username, type, id) {
    this.id_login = document;
    this.username = username;
    this.type = type;
    this.id = id;
  }

  getUserId() {
    let uid = sessionStorage.getItem("uid");
    return uid;
  }

  login() {
    this.authenticated = true;
    const encLogin = this.simpleCrypto.encrypt(this.id_login);
    const encUsername = this.simpleCrypto.encrypt(this.username);
    sessionStorage.setItem("_id", this.id_login);
    sessionStorage.setItem("type", this.type);
    sessionStorage.setItem("uid", this.id);
    sessionStorage.setItem("username", this.username);
    sessionStorage.setItem("ctrldoc", encLogin);
    sessionStorage.setItem("ctrlusername", encUsername);
  }

  logout() {
    this.authenticated = false;
    sessionStorage.removeItem("_id");
    sessionStorage.removeItem("ctrldoc");
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("ctrlusername");
    sessionStorage.removeItem("type");
    sessionStorage.removeItem("uid");
  }

  isAdmin() {
    let type = sessionStorage.getItem("type");
    console.log(type);
    if (type !== "1") {
      return false;
    } else {
      return true;
    }
  }

  isAuthenticated() {
    const doc = sessionStorage.getItem("_id");
    const username = sessionStorage.getItem("username");
    if (doc !== null) {
      const ctrlDoc = sessionStorage.getItem("ctrldoc");
      const ctrlUs = sessionStorage.getItem("ctrlusername");
      const decDoc = this.simpleCrypto.decrypt(ctrlDoc);
      const decUs = this.simpleCrypto.decrypt(ctrlUs);

      this.authenticated = doc === decDoc && username === decUs ? true : false;
      return this.authenticated;
    } else {
      return false;
    }
  }
}

export default new Auth();
