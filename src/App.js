import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from "react-router-dom";
import './App.css';
//import Login from './components/login';
//import Home from './components/home';
//import Gestiones from './components/gestiones';
//import Reportes from './components/reportes';
//import Usuarios from './components/usuarios';
//import Resoluciones from './components/resoluciones';
//import {ProtectedRoute} from './middleware/protected.route';
//import FormularioUsuario from './components/formularioUsuario';
//import FormularioResolucion from './components/formularioResolucion';

//import CerrarSesion from './components/cerrarsesion';
import Tipo from './components/tipo/tipo';
import Tipos from './components/tipos/tipos';
import Machines from './components/machines/machines';
import Recipes from './components/recipes/recipes';
import FormTipos from './components/tipos/form_tipos';
import FormTipo from './components/tipo/form_tipo';
import Plans from './components/plans/plans';
import PlansMachines from './components/plans_machines/plans_machines';
import FormPlans from './components/plans/form_plans';
import FormMachine from './components/machines/form_machine';
import FormPM from './components/plans_machines/form_plans_machines';
import FormRecipe from './components/recipes/form_recipes';
import FormUsers from './components/users/form_users';
import Index from './components/index';
import Menu from './components/menu';
import Site from './components/mysite/mysite';
import MyRecipes from './components/myrecipes/myrecipes';
import Workout from './components/workout/workout';
import Users from './components/users/users';
import Close from './components/close_sesion';
import Unblock from './components/unblock/unblock';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="">
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Index} /> 
              <Route exact path="/tipo" component={Tipo} />
              <Route exact path="/tipo/add" component={FormTipo} />
              <Route exact path="/tipos/add" component={FormTipos} />
              <Route exact path="/machines/add" component={FormMachine} />
              <Route exact path="/recipes/add" component={FormRecipe} />
              <Route exact path="/plans/add" component={FormPlans} />
              <Route exact path="/users/add" component={FormUsers} />
              <Route exact path="/tipos" component={Tipos} />
              <Route exact path="/machines" component={Machines} />
              <Route exact path="/recipes" component={Recipes} />
              <Route exact path="/plans" component={Plans} />
              <Route exact path="/mysite" component={Site} />
              <Route exact path="/myrecipes" component={MyRecipes} />
              <Route exact path="/plans_machines" component={PlansMachines} />
              <Route exact path="/plan:machines/add" component={FormPM} />
              <Route exact path="/menu" component={Menu} />              
              <Route exact path="/users" component={Users} />              
              <Route exact path="/workout" component={Workout} />              
              <Route exact path="/close" component={Close} />              
              <Route exact path="/unblock" component={Unblock} />              
              {/*<ProtectedRoute exact path="/home" component={Home} /> >
              <ProtectedRoute exact path="/gestion" component={Gestiones} /> >
              <ProtectedRoute exact path="/reportes" component={Reportes} /> >
              <ProtectedRoute exact path="/usuarios" component={Usuarios} /> >
              <ProtectedRoute exact path="/resoluciones" component={Resoluciones} /> >
              <ProtectedRoute exact path="/resoluciones/crear" component={FormularioResolucion} /> >
              <ProtectedRoute exact path="/usuarios/crear" component={FormularioUsuario} /> >              
    <ProtectedRoute exact path="/cerrarsesion" component={CerrarSesion} /> >  */}            
            </Switch>
          </BrowserRouter>
        </div>
      </div>
    );
  }
}

export default App;
